# DiscordGrapher

## Setup Locally

Setting up a development envrionment locally is straight-forward:

1. clone this repo locally `git clone git@gitlab.com:markypooch/discordgrapher.git`

1. copy `.env.example` to `.env`, review and optionally override options

1. run `docker-compose up`

1. browse to `http://127.0.0.1/`



## Conventions

### mypy

Try to use type annotations where sensible/possible, and where you think it'll assist readability (lots or arguments to a method)
Offline static type checking will be added at some point
