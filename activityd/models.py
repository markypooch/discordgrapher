from activityd import db

class Activity(db.Model):
    id    = db.Column(db.String(64), primary_key=True)
    username = db.Column(db.String(64))
    messages_total = db.Column(db.Integer)
    messages_last_week = db.Column(db.Integer)

class Messagecounts(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    datestr       = db.Column(db.String(64))
    message_count = db.Column(db.Integer)
    graph_id      = db.Column(db.String)

class Channels(db.Model):
    id  = db.Column(db.Integer, primary_key=True)
    message_count = db.Column(db.Integer)
    channel_name = db.Column(db.String(64))
    graph_id = db.Column(db.String)