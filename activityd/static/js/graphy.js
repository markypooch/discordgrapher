window.onload = function(e) {

    var graphId = window.location.href.split("/")[4];

    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() {
        console.log(this.status + " : " + this.statusText);

        if (this.readyState > 3 && this.status == 200) {
            jsonPayload = JSON.parse(this.responseText);

            graph = document.getElementById('graph');
            Plotly.plot( graph, [{
                x: jsonPayload.date_str,
                y: jsonPayload.cnt}], {
                    margin: { t: 0 }
                }
            );

            pie = document.getElementById('pie');
            Plotly.plot(pie, [{
                values: jsonPayload.perc,
                labels: jsonPayload.perc_lab,
                type: "pie"
            }]);
        }
        else if (this.readyState > 3 && (this.status == 201 || this.status == 500)) {
            jsonPayload = JSON.parse(this.responseText);
            document.getElementById("graph").innerHTML = jsonPayload.message;
        }
    };

    console.log(graphId);
    xhttp.open("GET", `${window.API_ROOT}/api/getusage/${graphId}/`);
    xhttp.send();
};
