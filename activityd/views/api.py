from activityd import app, db
from activityd.models import Activity, Messagecounts, Channels
from flask import request, make_response, Response
from datetime import datetime

import json

@app.route("/api/consumeusage", methods=['POST', 'GET'])
def consume_usage() -> Response:
    http_response : Response
    try:
        json_payload = request.json

        # does this user already exist?
        activity = Activity.query.filter_by(id=json_payload['userId']).first()
        if not activity:

            # no, log their user id, and name
            activity = Activity()
            activity.id        = json_payload['userId']
            activity.username  = json_payload.get('name')
        else:

            # delete any old messages saved in the db associated with this user
            Messagecounts.query.filter_by(graph_id=json_payload['userId']).delete()
            Channels.query.filter_by(graph_id=json_payload('userId')).delete()
            db.session.commit()

        # set their message totals, and their last week count
        activity.messages_total     = json_payload.get('messages_total')
        activity.messages_last_week = json_payload.get('messages_last_week')

        db.session.add(activity)

        # get our fresh messages (hot off the stove) and add them to our session
        for key, value in json_payload['messages_per_day'].items():
            message_counts = Messagecounts(graph_id=json_payload.get('userId'),
                                           datestr=datetime.utcfromtimestamp(int(float(key))).strftime('%Y-%m-%d'),
                                           message_count=value)
            db.session.add(message_counts)

        for key, value in json_payload['channels'].items():
            channel = Channels(graph_id=json_payload.get('userId'),
                               channel_name=key,
                               message_count=value)
            db.session.add(channel)

        db.session.commit()
        http_response = make_response(json.dumps({"url":f"/graphs/{json_payload['userId']}"}), 200)
    except Exception as exception:
        http_response = make_response(json.dumps({"error":f"{repr(exception)}"}), 500)

    return http_response

@app.route('/api/getusage/<graph_id>', methods=['GET', 'POST', 'DELETE', 'PUT', 'OPTIONS'])
def retrieve_graph_data(graph_id):
    http_response : Response
    try:
        activity = Activity.query.filter_by(id=graph_id).first()
        message_counts = Messagecounts.query.filter_by(graph_id=graph_id).all()
        channels       = Channels.query.filter_by(graph_id=graph_id).all()

        if not activity:
            http_response = make_response(json.dumps({"message":"No graphable data found with supplied ID"}), 201)
        else:
            graph = {}
            graph['date_str'], graph['cnt'], graph['perc'], graph['perc_lab'] = [], [], [], []

            # Reorganize the data before responding to the client. This way it can just be
            # fed straight into Plotly
            for message in message_counts:
                graph['date_str'].append(message.datestr)
                graph['cnt'].append(message.message_count)

            for channel in channels:
                graph['perc'].append((channel.message_count/activity.messages_total)*100)
                graph['perc_lab'].append(channel.channel_name)

            graph['name']           = activity.username
            graph['messages_total'] = activity.messages_total

            http_response = make_response(json.dumps(graph), 200)

        http_response.headers['Access-Control-Allow-Origin'] = '*'
    except Exception as exception:
        http_response = make_response(json.dumps({"message":f"{repr(exception)}"}), 500)

    return http_response
