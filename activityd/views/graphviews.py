from activityd import app
from flask     import Response, make_response, render_template, redirect, request


@app.route("/", methods=['POST', 'GET'])
def index() -> Response:

    return redirect("/home")


'''
TODO
'''
@app.route("/home", methods=['GET'])
def home_view() -> Response:
    return redirect("/graphs/nada")


@app.route("/graphs/<graph_id>", methods=['GET', 'POST'])
def my_graph(graph_id):
    http_response : Response
    try:
        http_response = make_response(render_template('graph.html', error=False), 200)

    except Exception as exception:
        http_response = make_response(render_template('graph.html',
            error=True,
            error_payload=f"500 Internal server error: {repr(exception)}"),
            500)

    return http_response

'''
TODO
'''
@app.route("/graph_builder", methods=['POST', 'GET'])
def graph_form_builder() -> Response:
    pass
