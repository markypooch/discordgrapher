FROM python:3.6

RUN mkdir /activityd
COPY . /activityd
WORKDIR /activityd

RUN pip install -r requirements.txt
